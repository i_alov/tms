 unit Cell;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;
type TCell = class
  private
  content:char;
  shape:TShape;
  public
  procedure setContent(c:char);
  function getContent():char;
end;

implementation
	procedure TCell.setContent(c:char);
  begin
    self.content := c;
  end;
  function TCell.getContent():char;
  begin
    result := self.content;
  end;

end.

