unit CellList;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Cell;

type
  TCellList = class
    type
  		PNode = ^TNode;
  		TNode = record
        index: integer;
        content: TCell;
        prev: PCell;
        next: PCell;
      end;
    private
      FirstNode: PNode;
      LastNode: PNode;
      CurrentNode: PNode;
      function checkIfEmpty():boolean;
      function getSize():integer;
    public
			procedure Append(Cell: TCell);
      procedure Delete(index: integer); overload;
      procedure Delete(Cell: TCell); overload;
      function getContent(index: integer):TCell;
  end;

implementation

function TCellList.checkIfEmpty():bool;
	begin
    if(self.FirstNode == nil) then result:= true
    else result:=false;
  end;

function TCellList.getSize():integer;
  var count: integer;
    TempNode: PNode;
  begin
    count:= 0;
  	if (checkIfEmpty() <> false) then
    	begin
        TempNode:= CurrentNode;
        CurrentNode:= FirstNode;
        while (CurrentNode^.next <> nil) do begin
          count +=1
          CurrentNode:= CurrentNode^.next;
        end;
      end;
    CurrentNode:= TempNode;
    result:= count;
  end;

function TCellList.getContent(index: integer): TCell;
  var tempNode: PNode;
	begin
    if (checkIfEmpty() <> false) then
      begin
        TempNode:= CurrentNode;
        CurrentNode:= FirstNode;
        while (CurrentNode^.index <> index) do CurrentNode:= CurrentNode^.next;
      end;
  end;


procedure TCellList.Append(Cell: TCell);
	begin
    if (checkIfEmpty <> false) then
  end;

end.

